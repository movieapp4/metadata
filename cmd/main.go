package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/movieapp4/metadata/internal/controller/metadata"
	httphandler "gitlab.com/movieapp4/metadata/internal/handler/http"
	"gitlab.com/movieapp4/metadata/internal/repository/memory"
	"gitlab.com/movieapp4/pkg/discovery"
	"gitlab.com/movieapp4/pkg/discovery/consul"
)

const serviceName = "metadata"

func main() {
	var port int
	flag.IntVar(&port, "port", 8081, "API handler port")
	flag.Parse()
	log.Printf("Starting the metadata service on port %d\n", port)

	registry, err := consul.NewRegistry("localhost:8500")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	instanceID := discovery.GenerateInstanceID(serviceName)

	if err := registry.Register(ctx, instanceID, serviceName, fmt.Sprintf("localhost:%d", port)); err != nil {
		panic(err)
	}

	go func() {
		for {
			if err := registry.ReportHealthyState(ctx, instanceID, serviceName); err != nil {
				log.Printf("failed to report healthy state: %s", err.Error())
			}
			time.Sleep(1 * time.Second)
		}
	}()

	defer registry.Deregister(ctx, instanceID, serviceName)

	repo := memory.NewRepository()
	ctrl := metadata.NewController(&repo)
	h := httphandler.NewHandler(&ctrl)

	http.Handle("/metadata", http.HandlerFunc(h.GetMetadata))

	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil); err != nil {
		panic(err)
	}
}

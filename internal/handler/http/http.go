package http

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"gitlab.com/movieapp4/metadata/internal/controller/metadata"
	"gitlab.com/movieapp4/metadata/internal/repository"
)

type Handler struct {
	ctrl *metadata.Controller
}

// NewHandler created a new movie metadata HTTP Handler
func NewHandler(c *metadata.Controller) *Handler {
	return &Handler{ctrl: c}
}

func (h *Handler) GetMetadata(w http.ResponseWriter, req *http.Request) {
	id := req.FormValue("id")
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx := req.Context()

	m, err := h.ctrl.Get(ctx, id)
	if err != nil && errors.Is(err, repository.ErrNotFound) {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		log.Printf("repository get error: %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	if err := json.NewEncoder(w).Encode(m); err != nil {
		log.Printf("Response encode error: %v\n", err)
	}
}

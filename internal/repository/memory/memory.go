package memory

import (
	"context"
	"sync"

	"gitlab.com/movieapp4/metadata/internal/repository"
	"gitlab.com/movieapp4/metadata/pkg/model"
)

// Repository defines a memory movie metadata repository
type Repository struct {
	sync.RWMutex
	data map[string]*model.MetaData
}

// New creats a new memory repository
func NewRepository() Repository {
	return Repository{data: make(map[string]*model.MetaData)}
}

// Get retrieves movie metadata for by movie metadata
func (r *Repository) Get(_ context.Context, id string) (*model.MetaData, error) {

	r.RLock()
	defer r.Unlock()

	v, ok := r.data[id]
	if !ok {
		return nil, repository.ErrNotFound
	}

	return v, nil
}

func (r *Repository) Put(_ context.Context, id string, data *model.MetaData) error {
	r.RLock()
	defer r.Unlock()

	r.data[id] = data

	return nil
}
